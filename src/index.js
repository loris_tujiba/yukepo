import React from 'react';
import ReactDOM from 'react-dom';
import Home from './pages/home.jsx';
import NoMatch from './pages/no-match.jsx';
import Tmc from './pages/tmc.jsx';
import Pp from './pages/pp.jsx';
import Karir from './pages/karir.jsx';
import ReduxPromise from 'redux-promise';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import reducers from './reducers/index.js';
import './app.css';

const store = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(

	<Provider store={store(reducers)}>
		<BrowserRouter>
			<Switch>
				<Route path="/karir" exact component={Karir}/>  
				<Route path="/pp" exact component={Pp}/>  
				<Route path="/tmc" exact component={Tmc}/>  
  				<Route path="/" exact component={Home}/>  				
  				<Route component={NoMatch}/>
  			</Switch>
		</BrowserRouter>
	</Provider>
  ,document.querySelector('#root')
 );
