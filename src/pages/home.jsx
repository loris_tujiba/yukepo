import React, { Component } from 'react';
import { Grid, Row } from 'react-bootstrap';
import Header from '../components/header';
import Footer from '../components/footer';
import Headline from '../containers/headline.js';
import TrendingList from '../containers/trending-list.js';
import TopList from '../containers/top-list.js';
import NewestList from '../containers/newest-list.js';
import Ad from '../containers/ad.js';

class Home extends Component{

	render(){
		return(
			<div>
				<Header />
				<div id="wrapper">
					<Grid>
						<Row>
							<Headline image="http://fightersgeneration.com/nz6/game/tekken7fr/tekken7-fated-retribution-screenshot43.png"/>
							<Ad />
							<TrendingList />
							<hr className={"separator"}/>
							<TopList />
							<NewestList />
						</Row>
					</Grid>
				</div>
				<Footer />
			</div>
		)
	}
}

export default Home;
