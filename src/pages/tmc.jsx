import React, { Component } from 'react';
import { Grid, Breadcrumb } from 'react-bootstrap';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';

class Tmc extends Component{

	render(){
		return(
			<div>
				<Header />
				<Grid fluid className="tmc">
					<Breadcrumb>
					    <Breadcrumb.Item href="/">
					      Home
					    </Breadcrumb.Item>
					    <Breadcrumb.Item active>
					      Syarat dan Ketentuan
					    </Breadcrumb.Item>
					</Breadcrumb>	
					<h1>SYARAT &amp; KETENTUAN</h1>
					<p>
						Mohon untuk membaca dengan cermat kebijakan privasi ini sebelum registrasi.
						Terima kasih atas kunjungan anda ke situs YuKepo milik PT. Media Mulia Nusantara. Kami selalu berusaha yang terbaik untuk melindungi informasi pribadi milik Anda yang bersifat pribadi dan rahasia. Terdapat syarat dan ketentuan mengenai kebijakan privasi. Dengan bergabungnya Anda dengan YuKepo Anda telah setuju dan menerima syarat dan ketentuan berlaku dari YuKepo.
					</p>
					<h2>Registrasi</h2>
					<p>
						Dengan registrasi Anda menyatakan bahwa semua informasi dan perubahan dalam akun anda di YuKepo adalah akurat. YuKepo menyatakan tidak bertanggung jawab atas segala akibat yang terjadi dengan pemberian informasi
						atau perubahan. Informasi yang diperlukan:
						Nama Lengkap, Nama Akun, Jenis Kelamin, Alamat Email, No HP atau No Telepon
					</p>
					<h2>Tujuan Pengumpulan Data</h2>
					<p>
						Pengumpulan data dari penggunna digunakan untuk meningkatkan layanan YuKepo dan apabila terdapat aktivitas
						kriminal YuKepo akan mengungkapkan ke pihak yang berwajib.
					</p>
					<h2>Kerahasiaan Informasi</h2>
					<p>
						YuKepo memastikan bahwa semua data yang kami terima dari segala aktivitas yang dilakukan via YuKepo dijamin kerahasiaannya. Kami tidak akan menggunakan data tersebut dan membocorkan ke-pihak ketiga untuk hal-hal yang dapat merugikan pelanggan atau pengguna, terkecuali apabila ada aktivitas yang berbau kriminal, maka YuKepo berhak menggunakan data tersebut dan melaporkannya kepihak yang berwajib.
					</p>						
					<h2>Modifikasi Kebijakan</h2>
					<p>
						Dengan mengakses YuKepo, maka kami memiliki wewenang untuk memodifikasi website tanpa memberikan notifikasi pada pengguna kami.
					</p>
					<h2>Hukum</h2>
					<p>
						Syarat dan kebijakan privasi maupun semua aktiviatas YuKepo tunduk pada peraturan Negara Kesatuan Republik
						Indonesia.
					</p>
				</Grid>
				<Footer />
			</div>
		)
	}

}

export default Tmc;