import React, { Component } from 'react';
import { Grid } from 'react-bootstrap';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';

class NoMatch extends Component{

	render(){
		return(
			<div>
				<Header />
				<Grid fluid className="no-match">
					<div>
						<h1>Ups!</h1>
						<h2>Halaman yang anda cari tidak ditemukan</h2>	
					</div>					
				</Grid>
				<Footer />
			</div>
		)
	}

}

export default NoMatch;