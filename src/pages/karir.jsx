import React, { Component } from 'react';
import { Grid, Breadcrumb } from 'react-bootstrap';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';

class Karir extends Component{

	render(){
		return(
			<div>
				<Header />
				<Grid fluid className="tmc">
					<Breadcrumb>
					    <Breadcrumb.Item href="/">
					      Home
					    </Breadcrumb.Item>
					    <Breadcrumb.Item active>
					      Karir
					    </Breadcrumb.Item>
					</Breadcrumb>	
					<h1>Karir</h1>
					<p>
						YuKepo.com mengedepankan team diversity, kami menghargai perbedaan, karena kami yakin perbedaan dapat melahirkan ide yang cemerlang karena ide tersebut merupakan kombinasi dari ide-ide yang cemerlang. Selain itu kami juga menerapkan kerja yang fun di kantor karena kita ingin membuat orang tertawa ketika membaca website kita. Oleh karena itu, Anda bisa menentukan kapan harus break dan harus kerja sehingga dapat meningkatkan semangat dan efektifitas kerja.
					</p>
					<h2>Kesempatan</h2>
					<p>
						Dengan bergabung YuKepo.com, Anda bisa mendapatkan pengalaman untuk bekerjasama dengan kolega yang berasal dari lulusan Amerika seperti dari Ivy League University, Johns Hopkins University, and UC Berkeley. Selain itu juga dapat bekerjasama dari lulusan universitas ternama di Indonesia. Anda akan tergabung menjadi satu bersama tim yang ambisius untuk dapat menjadi game changer di industri media kususnya sektor humor dan bersama membangun website humor terbesar untuk millenials di Indonesia.
					</p>
				</Grid>
				<Footer />
			</div>
		)
	}

}

export default Karir;