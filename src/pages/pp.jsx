import React, { Component } from 'react';
import { Grid, Breadcrumb } from 'react-bootstrap';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';

class Pp extends Component{

	render(){
		return(
			<div>
				<Header />
				<Grid fluid className="tmc">
					<Breadcrumb>
					    <Breadcrumb.Item href="/">
					      Home
					    </Breadcrumb.Item>
					    <Breadcrumb.Item active>
					      Kebijakan Privasi
					    </Breadcrumb.Item>
					</Breadcrumb>	
					<h1>KEBIJAKAN PRIVASI</h1>
					<p>
						Dengan akses website YuKepo.com Anda menyetujui untuk memenuhi syarat dan ketentuan dari YuKepo.com
						meskipun Anda belum membaca kalimat ini. YuKepo.com berusaha untuk memastikan semua konten legal, akan
						tetapi bila terdapat konten yang tidak dapat terdeteksi YuKepo.com, maka pelanggaran yang menyangkut etika
						jurnalis atau hukum sepenuhnya tanggungan user. Terdapat beberapa poin:
					</p>
					<h2>Konten yang Dilarang</h2>
					<p>
						Terdapat guidelines dalam memposting konten di YuKepo.com: Konten yang dilarang adalah: Illegal, Pornografi, ancaman, mempermalukan suatu pihak, memprovokasi untuk melakukan tindakan kekerasan, spam, personal dan sesuatu rahasia
					</p>
					<h2>Tindakan yang Dilarang</h2>
					<p>
						Berkampanye melalu YuKepo.com, Interferensi sistem YuKepo.com Membuat akun yang lebih dari satu untuk
						menghindari hukuman atau larangan dari YuKepo.com
					</p>
					<h2>Kepoditor yang Dilarang</h2>
					<p>
						Seorang kepoditor adalah panggilan seorang contributor yang memposting di YuKepo. Setiap kepoditor dapat
						memanage kontennya sendiri dengan mengikuti guidelines YuKepo. Kepoditor tidak diizinkan melakukan tindakan-tindakan dibawah ini di website YuKepo: Aktivitas Illegal, repost informasi yang sudah dihapus, ,menghina, bersifat kasar, dan menyerang dengan kata-kata yang tidak senonoh, memulai perang kata-kata, memprovokasi pengguna lainnya untuk menyudutkan satu pihak
					</p>
					<h2>Laporan</h2>
					<p>
						Laporkanlah sebuah konten atau posting yang melanggar. Jangan melaporkan sebuah konten yang pengguna tidak suka.
					</p>
					<h2>Postingan Baru</h2>
					<p>
						Pastikan konten anda tidak hoaxes, spamming, dan plagiat. YuKepo dalam waktu singkat dapat mengakibatkan postingan dan akun anda terblokir, diharapkan menulis judul dengan Bahasa Indonesia yang benar dan baik, apabila konten tersebut diambil dari artikel online atau penulis lain diharapkan mencantumkan sumbernya. Apabila melakukan plagiarisme segala tanggunan hukum akan di tanggung oleh pengguna, dan postingan akan dihapus dari YuKepo.
					</p>
					<h2>Komentar</h2>
					<p>
						Komentar seperti lol, haha, tidak menambah nuansa dalam diskusi. Pengguna disarankan untuk memberikan komentar yang original yang dapat memperkaya diskusi.
					</p>
				</Grid>
				<Footer />
			</div>
		)
	}

}

export default Pp;