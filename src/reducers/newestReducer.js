import { FETCH_NEWEST } from '../actions';
import _ from 'lodash';

export default function(state=[], action){
	switch(action.type){
		case FETCH_NEWEST :
			return  _.merge({},state,_.mapKeys(action.payload.data, 'id'))
		default :
			return state;
	}
}
