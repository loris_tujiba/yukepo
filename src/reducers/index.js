import { combineReducers } from 'redux';
import postsReducer from './postsReducer';
import newestReducer from './newestReducer';

const rootReducers = combineReducers({
	posts : postsReducer,
	newest : newestReducer
});

export default rootReducers;
