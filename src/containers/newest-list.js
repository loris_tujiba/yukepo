import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchNewest } from '../actions';
import _ from 'lodash';
import { Grid, Row } from 'react-bootstrap';
import NewestItem from '../components/newest-item';
import LazyLoad from 'react-lazyload';

class NewestList extends Component{

	constructor(props){
    super(props);

    this.handleScroll = this.handleScroll.bind(this);
  }
	componentWillMount(){
		this.props.fetchNewest('tujiba');
	}

	componentDidMount() {
      window.addEventListener("scroll", this.handleScroll);
    }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll() {
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight-300) {
      this.props.fetchNewest('123')
    }
  }

	renderLists(){
		console.log(this.props.newest)
		var arr = _.values(this.props.newest);
		let chunkedarr = _.chunk(arr,3)
		return chunkedarr.map((newe,i) =>{
			return(
				<Row key={i}>
					{
						newe.map(n =>{
							return(
								<LazyLoad key={n.id} height={300} once>
								  <NewestItem title={n.id} username={n.categories} image={"http://lettuceevolve.com/wp-content/uploads/2013/05/placeholder.png"}/>
								</LazyLoad>

							)
						})
					}
				</Row>
			)
		})
	}

	render(){
		if(!this.props.newest){
			return(
				<div>Loading...</div>
			)
		}

		return(
			<div className="newest-container">
				<div className={"separation-heading newest"}>
					<h1>TERBARU</h1>
				</div>
				<Grid>
					{
						this.renderLists()
					}
				</Grid>
			</div>
		)
	}
}

function mapStateToProps(state){
	return{
		newest : state.newest
	}
}

export default connect(mapStateToProps, { fetchNewest })(NewestList);
