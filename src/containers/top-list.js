import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';
import _ from 'lodash';
import OwlCarousel from 'react-owl-carousel';
import TopItem from '../components/top-item';

class TopList extends Component{

	constructor(props){
		super(props);
		this.state = {
			options : {
				mouseDrag : true,
				touchDrag : true,
				dots : false,
				responsive:{
					0:{
						items : 1,
						stagePadding : 0
					},
					315:{
						items : 1,
						stagePadding : 20,
					},
					360:{
						items : 1,
						stagePadding : 40,
					},
					400:{
						items : 1,
						stagePadding : 60,
					},
					430:{
						items : 1,
						stagePadding : 80,
					},
					480:{
						items : 1,
						stagePadding : 100,
					},
					510:{
						items : 1,
						stagePadding : 120,
					},
					540:{
						items : 1,
						stagePadding : 140,
					},
					580:{
						items : 2,
						stagePadding : 0
					},
					650:{
						items : 2,
						stagePadding : 20
					},
					700:{
						items : 2,
						stagePadding : 40
					},
					767:{
						items : 2,
						stagePadding : 80
					},
					830:{
						items : 3,
						stagePadding : 0
					},
					992:{
						items : 2,
						stagePadding : -50
					}
				}
			}
		}
	}

	componentWillMount(){
		this.props.fetchPosts();
	}

	renderLists(){
		return _.map(this.props.posts, post =>{
			return(
					<TopItem key={post.id} title={post.company.catchPhrase} name={post.name} username={post.username} email={post.email} image={"https://s3.amazonaws.com/37assets/svn/1065-IMG_2529.jpg"}/>
			)
		});
	}

	render(){
		if(!this.props.posts){
			return(
				<div>Loading...</div>
			)
		}

		return(
			<div className="top-container">
				<div className={"separation-heading"}>
					<i className ={"yu-topabis"}></i>
					<h1>TOP ABIS</h1>
				</div>

				<OwlCarousel
					className="owl-theme"
					{...this.state.options}
				>
					{
						this.renderLists()
					}

				</OwlCarousel>
			</div>
		)
	}
}

function mapStateToProps(state){
	return{
		posts : state.posts
	}
}

export default connect(mapStateToProps, { fetchPosts })(TopList);
