import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';
import _ from 'lodash';
import { Grid, Row } from 'react-bootstrap';
import TrendingItem from '../components/trending-item';

class TrendingList extends Component{

	componentWillMount(){
		this.props.fetchPosts();
	}

	renderLists(){
		let chunkedarr = _.chunk(this.props.posts,3)
		return _.map(chunkedarr, (posts,i) =>{
			return(
				<Row key={i}>
					{
						posts.map(post =>{
							return(
								<TrendingItem key={post.id} title={post.company.catchPhrase} name={post.name} image={"http://lettuceevolve.com/wp-content/uploads/2013/05/placeholder.png"}/>
							)
						})
					}
				</Row>
			)
		})
	}

	render(){
		if(!this.props.posts){
			return(
				<div>Loading...</div>
			)
		}

		return(
			<div className="trending-container">
				<Grid>
					{
						this.renderLists()
					}
				</Grid>
			</div>
		)
	}
}

function mapStateToProps(state){
	return{
		posts : state.posts
	}
}

export default connect(mapStateToProps, { fetchPosts })(TrendingList);
