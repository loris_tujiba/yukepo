import React, { Component } from 'react';
import { Jumbotron, Grid, Row, Col } from 'react-bootstrap';

class Headline extends Component{

	render(){
		var bgImage = {
			backgroundImage : 'url('+this.props.image+')'
		}

		return(
			<div className="headline">
				<Jumbotron className="headline-img" style={bgImage}>
					<div className={"headline-header"}>
						<i className={"yu-flash"}></i>
						<span className={"headline-tag"}>
							TRENDING
						</span>
					</div>
				</Jumbotron>
				<Grid>
					<Row>
						<Col xs={12} md={12} lg={12} className={"headline-body"}>
							<h1 className={"headline-title"}>Diblokir Kominfo, ini tanggapan CEO</h1>
						</Col>
					</Row>
					<Row>
						<Col xs={12} md={12} lg={12} className={"headline-footer"}>
							<h6 className={"timestamps"}>2 HARI YANG LALU</h6>|<h6 className={"poster"}>Resha Leo</h6>
						</Col>
					</Row>
				</Grid>
			</div>
		)
	}

}

export default Headline;
