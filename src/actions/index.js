import axios from 'axios';

export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_NEWEST = 'FETCH_NEWEST';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';

export function fetchPosts(){
	const req = axios.get('http://jsonplaceholder.typicode.com/users');
	return {
		type: FETCH_POSTS,
		payload : req
	};
}

export function fetchNewest(API_KEY){
	const req = axios.get(`${ROOT_URL}/posts?key=${API_KEY}`);
	return{
		type: FETCH_NEWEST,
		payload : req
	};
}
