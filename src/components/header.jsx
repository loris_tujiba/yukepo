import React, { Component } from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, FormGroup, FormControl, Form, ControlLabel, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import logo from '../assets/icons/drawable-xhdpi/yukepo.png';

class Header extends Component {

  componentDidMount() {
    $(function(){
      $('#search-toggle').click(function () {
        if($('#menu').is(":visible")){

        }else{
          $('#search').show("fast")
          $('#menu-toggle').hide()
          $('#close-search').show()
        }
      });
      $('#close-search').click(function () {
        $('#search').hide("fast")
        $('#close-search').hide()
        $('#menu-toggle').show()
      });
    });
  }

  render() {
    return (
      <Navbar collapseOnSelect fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">
              <img alt="yukepo" src={logo}/>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle id="menu-toggle"/>
          <Button id="close-search" className="close-search"></Button>
          <Button id="search-toggle" className="search-toggle"></Button>
          <Nav className="nav-media">
            <NavItem>
              <i className="ico-nv yu-facebook"></i>
              <i className="ico-nv yu-instagram"></i>
              <i className="ico-nv yu-twitter"></i>
              <i className="ico-nv yu-youtube"></i>
            </NavItem>
            <NavItem>
              <Button bsStyle="primary" type="submit">BUAT ARTIKEL</Button>
            </NavItem>
          </Nav>
        </Navbar.Header>
        <Navbar.Collapse id="menu">
          <Nav pullLeft>
            <NavItem eventKey={1}>
              <i className="ico-dl yu-trending"></i> TRENDING
            </NavItem>
            <NavDropdown eventKey={2} title="HIBURAN" id="hiburan-nav-dropdown">
              <MenuItem eventKey={2.1}>
                <div className="ico-dl"><i className="ico-dl yu-travel"></i></div> TRAVEL
              </MenuItem>
              <MenuItem eventKey={2.1}>
                <div className="ico-dl"><i className="ico-dl yu-film"></i></div> FILM
              </MenuItem>
              <MenuItem eventKey={2.2}>
                <div className="ico-dl"><i className="ico-dl yu-tips"></i></div> TIPS
              </MenuItem>
              <MenuItem eventKey={2.3}>
                <div className="ico-dl"><i className="ico-dl yu-olahraga"></i></div> OLAHRAGA
              </MenuItem>
              <MenuItem eventKey={2.4}>
                <div className="ico-dl"><i className="ico-dl yu-life"></i></div> LIFE
              </MenuItem>
              <MenuItem eventKey={2.5}>
                <div className="ico-dl"><i className="ico-dl yu-indonesiaku"></i></div> INDONESIAKU
              </MenuItem>
              <MenuItem eventKey={2.6}>
                <div className="ico-dl"><i className="ico-dl yu-kreatif"></i></div> KREATIF
              </MenuItem>
              <MenuItem eventKey={2.6}>
                <div className="ico-dl"><i className="ico-dl yu-selebritis"></i></div> SELEBRITIS
              </MenuItem>
              <MenuItem eventKey={2.6}>
                <div className="ico-dl"><i className="ico-dl yu-unik"></i></div> UNIK
              </MenuItem>
              <MenuItem eventKey={2.6}>
                <div className="ico-dl"><i className="ico-dl yu-gambarlucu"></i></div> GAMBAR LUCU
              </MenuItem>
              <MenuItem eventKey={2.6}>
                <div className="ico-dl"><i className="ico-dl yu-mistis"></i></div> MISTIS
              </MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={3} title="MEDIA SOSIAL" id="media-sosial-nav-dropdown">
              <MenuItem eventKey={3.1}>
                <i className="fa fa-facebook"></i> FACEBOOK
              </MenuItem>
              <MenuItem eventKey={3.2}>
                <i className="fa fa-twitter"></i> TWITTER
              </MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={4} title="DUNIA" id="dunia-nav-dropdown">
             <MenuItem eventKey={3.1}>
                <i className="fa fa-snapchat-ghost"></i> EROPA
              </MenuItem>
              <MenuItem eventKey={3.2}>
                <i className="fa fa-snapchat-ghost"></i> ASIA
              </MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={4} title="YUKEPO TV" id="yukepo-nav-dropdown">
              <MenuItem eventKey={3.1}>
                <i className="fa fa-snapchat-ghost"></i> BERITA SATU
              </MenuItem>
              <MenuItem eventKey={3.2}>
                <i className="fa fa-snapchat-ghost"></i> BERITA DUA
              </MenuItem>
            </NavDropdown>
          </Nav>
          <Navbar.Form pullRight>
            <FormGroup>
              <Button bsStyle="primary" bsSize="small" className="btn-login" type="submit"><i className="yu-user"></i>LOGIN</Button>
            </FormGroup>
          </Navbar.Form>
          <Navbar.Form className="search-desktop" pullRight>
            <Form>
              <FormGroup>
                <FormControl type="text" placeholder="SEARCH..." />
              </FormGroup>
              <Button bsStyle="primary" type="submit"><i className="fa fa-search fa-lg"></i></Button>
            </Form>
          </Navbar.Form>
        </Navbar.Collapse>
        <Navbar id="search">
          <Navbar.Form pullLeft>
            <Form>
              <h1>Search</h1>
              <FormGroup>
                <FormControl type="text" placeholder="APA YANG MAU KAMU CARI?" />
              </FormGroup>
              <Button bsStyle="primary" type="submit"><i className="fa fa-search fa-lg"></i></Button>
            </Form>
          </Navbar.Form>
        </Navbar>
      </Navbar>
    );
  }

}

export default Header;
