import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

class TopItem extends Component{
	render(){
		var bgImage = {
			backgroundImage : 'url('+this.props.image+')'
		}
		return(
			<div className="item top">
				<Row>
					<Col md={12} xs={12} className="top-thumb" style={bgImage}>
					</Col>
				</Row>
				<Row>
					<Col md={12} xs={12} className={"top-body"}>
						<div className={"top-title"}>
							<h2>{this.props.title}</h2>
						</div>
						<div className={"top-footer"}>
							<h6 className={"timestamps"}>2 HARI YANG LALU</h6> | <h6>{this.props.name}</h6>
						</div>
					</Col>
				</Row>
			</div>
		)
	}
}

export default TopItem;
