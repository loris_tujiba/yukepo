import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

class NewestItem extends Component{
	render(){
		var bgImage = {
			backgroundImage : 'url('+this.props.image+')'
		}
		return(
			<Col xs={12} md={12} lg={12} className="item newest">
				<Row>
					<Col md={6} xs={6} style={bgImage} className="item-thumb">
					</Col>
					<Col md={6} xs={6} lg={6} className={"item-body"}>
						<div className={"item-title"}>
							<h3>{this.props.title}</h3>
						</div>
						<div className={"item-category"}>
							<h5>{this.props.username}</h5>
						</div>
						<div className={"item-footer"}>
							<h6 className={"timestamps"}>14 JULY 2017</h6>|<h6>OLPHI DISYA</h6>
						</div>
					</Col>
				</Row>
			</Col>
		)
	}
}

export default NewestItem;
