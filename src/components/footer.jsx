import React, { Component } from 'react';
import { Grid, Row, Col, Form, FormGroup, FormControl,  Button } from 'react-bootstrap';

class Footer extends Component{

	render(){
		return(
			<footer>
				<Grid>
					<Row className="subscribe">
						<Col md={12} xs={12}>
							<h3>Update terus dengan artikel kami yuk!</h3>
							<h5>Dengan berlangganan newsletter kami melalui Email</h5>
						</Col>
					</Row>
					<Row className="subscribe-form">
						<Col md={12} xs={12}>
								<Form inline>
									<FormGroup className={"footer-form"}>
										<FormControl bsSize="small" type="text" placeholder="EMAIL KAMU" />
									</FormGroup>{' '}
									<Button bsSize="small"  bsStyle="primary" type="submit">LANGGANAN</Button>
								</Form>
						</Col>
					</Row>
					<Row className="about">
						<Col md={12} xs={12}>
							<div className={"footer-item"}>
								<a href="/">Tentang YuKepo</a>
								<a href="/">Kebijakan Provasi</a>
							</div>
						</Col>
					</Row>
					<Row className="tmc">
						<Col md={12} xs={12}>
							<div className={"footer-item"}>
								<a href="/">Syarat dan Ketentuan</a>
								<a href="/">Karir</a>
								<a href="/">Kontak YuKepo</a>
							</div>
						</Col>
					</Row>
					<Row className="copyright">
						<Col md={12} xs={12}>
							<div className={"footer-item"}>
								<h6>&copy; YuKepo. All rights reserved</h6>
							</div>
						</Col>
					</Row>
				</Grid>
			</footer>
		)
	}
}

export default Footer;
