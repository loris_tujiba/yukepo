import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

class TrendingItem extends Component{
	render(){
		var bgImage = {
			backgroundImage : 'url('+this.props.image+')'
		}

		return(
			<Col xs={12} md={12} lg={12} className="item trending">
				<Row>
					<Col md={5} xs={6} lg={5} style={bgImage} className="item-thumb">
					</Col>
					<Col md={7} xs={6} lg={7} className={"item-body"}>
						<div className={"item-header"}>
							<i className={"yu-flash"}></i>
							<span className={"item-tag"}>
								TRENDING
							</span>
						</div>
						<div className={"item-title"}>
							<h2>{this.props.title}</h2>
						</div>
						<div className={"item-footer"}>
							<h6 className={"timestamps"}>2 JAM YANG LALU</h6>|<h6>OLPHI DISYA</h6>
						</div>
					</Col>
				</Row>
			</Col>
		)
	}
}

export default TrendingItem;
