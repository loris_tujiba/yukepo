var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(){
	return gulp.src('./sass/app.scss')
		.pipe(sass())
		.pipe(gulp.dest('./src'));
});

gulp.task('watch', function(){
	gulp.watch('./sass/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'watch']);